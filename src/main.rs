extern crate approx;
extern crate clap;
extern crate lazy_static;
use approx::assert_relative_eq;
use clap::App;
use lazy_static::lazy_static;
use regex::Regex;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Seek;
use std::io::SeekFrom;

type BoxedLineIterator = Box<dyn Iterator<Item = String>>;

struct FileIterator {
    finished_preamble: bool,
    lines: BoxedLineIterator,
}

impl FileIterator {
    fn new(infile: File) -> FileIterator {
        let reader = BufReader::new(infile);
        let lines = reader.lines().map(|s| s.unwrap());
        FileIterator {
            finished_preamble: false,
            lines: Box::new(lines),
        }
    }
}

impl Iterator for FileIterator {
    type Item = String;

    // Emit gcode from the file without preamble or postamble.
    fn next(&mut self) -> Option<Self::Item> {
        while !self.finished_preamble {
            if self
                .lines
                .next()
                .expect("Could not find the end of the preamble, are you sure you added a comment?")
                .to_lowercase()
                .contains("preamble done")
            {
                self.finished_preamble = true;
            }
        }

        let line = self.lines.next();

        let text = line.expect("Could not find postamble comment.");
        let layer_re = Regex::new(r"^\s*;\s*LAYER:([1-9]+)\s*$").unwrap();
        if text.to_lowercase().contains("postamble") || layer_re.is_match(&text) {
            None
        } else {
            Option::Some(text)
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct Point {
    x: f32,
    y: f32,
}

#[derive(Copy, Clone, Debug)]
struct Size {
    x: f32,
    y: f32,
}

#[derive(Copy, Clone, Debug)]
struct Offset {
    x: f32,
    y: f32,
    z: f32,
}

static PEN_OFFSET: Offset = Offset {
    x: -54.0,
    y: 20.0,
    z: 0.9,
};

static ADJUST_OFFSET: Offset = Offset {
    x: 84.0,
    y: 62.0,
    z: 0.0,
};

static MINS: Offset = Offset {
    x: 0.0,
    y: 0.0,
    z: 0.9,
};

static MAXES: Offset = Offset {
    x: 160.0,
    y: 200.0,
    z: 50.0,
};

fn apply_offset(line: String, origin: Point, _size: Size) -> Option<String> {
    let s: Vec<&str> = line[..line.find(";").unwrap_or(line.len())]
        .trim()
        .split(" ")
        .collect();
    if s[0] != "G1" {
        return Some(line);
    }

    // Find all motion parameters ("X3.2", "Y1", etc).
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(\w)([\d\.]+)$").unwrap();
    }

    let mut axes = s[1..]
        .iter()
        .map(|x| x.chars().nth(0).unwrap())
        .collect::<Vec<char>>();
    axes.sort();
    if vec!["E", "EF", "S"].contains(&axes.into_iter().collect::<String>().as_str()) {
        return None;
    }

    let mut output: Vec<String> = vec![String::from(s[0])];

    for param in &s[1..] {
        let caps = RE.captures(param).unwrap();
        let axis: &str = &caps[1];
        let mut val = caps[2].parse::<f32>().unwrap();

        if axis == "X" {
            val = MINS
                .x
                .max(MAXES.x.min(val + PEN_OFFSET.x + ADJUST_OFFSET.x - origin.x));
        } else if axis == "Y" {
            val = MINS
                .y
                .max(MAXES.y.min(val + PEN_OFFSET.y + ADJUST_OFFSET.y - origin.y));
        } else if axis == "Z" {
            val = MINS
                .z
                .max(MAXES.z.min(val + PEN_OFFSET.z + ADJUST_OFFSET.z));
        } else if axis == "F" {
        } else {
            continue;
        }

        output.push(format!("{}{:.3}", axis, val));
    }
    if output.len() == 1 {
        return None;
    }

    Some(output.join(" "))
}

// Replace the preamble and postamble and return an iterator with the changed lines.
fn read_replaced_file(file: File) -> BoxedLineIterator {
    let iterator = FileIterator::new(file);

    let preamble_lines =
        BufReader::new(File::open("gcode/preamble.gcode").expect("Could not open preamble file."))
            .lines()
            .map(|s| s.unwrap());

    let postamble_lines = BufReader::new(
        File::open("gcode/postamble.gcode").expect("Could not open postamble_lines file."),
    )
    .lines()
    .map(|s| s.unwrap());

    return Box::new(preamble_lines.chain(iterator).chain(postamble_lines));
}

#[derive(Copy, Clone, Debug)]
struct Extents {
    origin: Point,
    size: Size,
}

fn find_extents(lines: BoxedLineIterator) -> Extents {
    /*
    TODO:
    filter each line to maybe x and maybe y
    ->
    accumulate maybe x and maybe y values into min/max values.

    conver min/max values into an Extents value and return it.
    */
    lazy_static! {
        static ref EXTRACT_XY_REGEX: Regex =
            Regex::new(
                r"^G0?[01][^XY]*(?:X(?P<x_value>\d*(?:\.\d+)?))?[^Y]*(?:Y(?P<y_value>\d*(?:\.\d+)?))?.*$",).unwrap();
    }

    fn function_or_none(a: Option<f32>, b: Option<f32>, f: fn(f32, f32) -> f32) -> Option<f32> {
        match a {
            None => b,
            Some(a_val) => match b {
                None => Some(a_val),
                Some(b_val) => Some(f(a_val, b_val)),
            },
        }
    }

    let min_max = lines
        .map(|s| match EXTRACT_XY_REGEX.captures(&s) {
            None => (None, None),
            Some(c) => (
                c.name("x_value")
                    .and_then(|x| x.as_str().parse::<f32>().ok()),
                c.name("y_value")
                    .and_then(|y| y.as_str().parse::<f32>().ok()),
            ),
        })
        .fold((None, None, None, None), |acc, xy| {
            (
                function_or_none(acc.0, xy.0, f32::max),
                function_or_none(acc.1, xy.0, f32::min),
                function_or_none(acc.2, xy.1, f32::max),
                function_or_none(acc.3, xy.1, f32::min),
            )
        });

    let origin = Point {
        x: min_max.1.unwrap(),
        y: min_max.3.unwrap(),
    };

    let size = Size {
        x: min_max.0.unwrap() - origin.x,
        y: min_max.2.unwrap() - origin.y,
    };

    return Extents {
        origin: origin,
        size: size,
    };
}

fn find_extents_from_file(file: File) -> Extents {
    return find_extents(Box::new(FileIterator::new(file)));
}

fn sanitized_gcode(mut file: File) -> BoxedLineIterator {
    let extents = find_extents_from_file(file.try_clone().unwrap());

    file.seek(SeekFrom::Start(0))
        .expect("Problem resetting file offset to 0.");

    let chain = read_replaced_file(file)
        .filter(|x| {
            !Regex::new(r"^(G92|M104|M109|M140|M190|M105|M106|M107)( .*)?$")
                .unwrap()
                .is_match(x.as_ref())
        })
        .map(move |x| apply_offset(x, extents.origin, extents.size))
        .filter(|x| x.is_some())
        .map(|x| x.unwrap());

    return Box::new(chain);
}

fn main() {
    let matches = App::new("sanitize_gcode")
        .version("0.1")
        .about("...sanitizes Gcode.")
        .author("Two Guys")
        .args_from_usage(
            "
            <INPUT>                   'Sets the input file to use'
            ",
        )
        .get_matches();

    let filename = matches.value_of("INPUT").unwrap();
    println!("Filename: {}", filename);

    let file = File::open(filename).expect("Could not open input file.");
    println!("Sanitizing...");
    let lines = sanitized_gcode(file);
    for line in lines {
        println!("{}", line);
    }
    println!("Done.");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_offset() -> Result<(), String> {
        let input_outputs = vec![
            ("G1 X82.015 Y78.238 E0.69749", "G1 X40.246 Y88.470"),
            ("G1 Z0.750 F4800.000", "G1 Z1.650 F4800.000"),
            ("G1 X80.117 Y79.956", "G1 X38.348 Y90.188"),
            ("G1 Z0.250", "G1 Z1.150"),
            ("G1 F600.000", "G1 F600.000"),
            ("G1 X82.015 Y78.238 E0.69749", "G1 X40.246 Y88.470"),
            ("G1 X84.739 Y76.249 E0.82594", "G1 X42.970 Y86.481"),
            ("G1 X86.960 Y74.959 E0.92374", "G1 X45.191 Y85.191"),
            ("G1 X89.178 Y73.924 E1.01692", "G1 X47.409 Y84.156"),
            ("G1 X91.584 Y73.052 E1.11438", "G1 X49.815 Y83.284"),
            ("G1 X94.872 Y72.238 E1.24334", "G1 X53.103 Y82.470"),
            ("G1 X97.402 Y71.887 E1.34059", "G1 X55.633 Y82.119"),
            ("G1 X99.885 Y71.768 E1.43526", "G1 X58.116 Y82.000"),
        ];

        for input_output in input_outputs {
            assert_eq!(
                apply_offset(
                    String::from(input_output.0),
                    Point {
                        x: 71.769,
                        y: 71.768,
                    },
                    Size {
                        x: 56.463,
                        y: 56.464,
                    },
                ),
                Some(String::from(input_output.1))
            );
        }

        assert_eq!(
            apply_offset(
                String::from("G1 E0.60000 F2400.00000"),
                Point {
                    x: 71.769,
                    y: 71.768,
                },
                Size {
                    x: 56.463,
                    y: 56.464,
                },
            ),
            None
        );

        Result::Ok(())
    }

    #[test]
    fn test_find_extents() -> Result<(), String> {
        let lines = vec![
            "G1 X12.233 Y11.623 E0.0166",
            "G1 X18.969 Y15.916 E19.59452",
            "G1 F2400 E0",
            "G1 Z20 F700",
            ";Generated with Cura_SteamEngine 4.4.1",
            "M105",
        ];

        let extents = find_extents(Box::new(lines.into_iter().map(|s| String::from(s))));

        assert_relative_eq!(extents.origin.x, 12.233);
        assert_relative_eq!(extents.origin.y, 11.623);
        assert_relative_eq!(extents.size.x, 6.736);
        assert_relative_eq!(extents.size.y, 4.293);

        Result::Ok(())
    }

    #[test]
    fn test_end_to_end() -> Result<(), String> {
        let actual_lines = sanitized_gcode(
            File::open("test_files/test_input.gcode").expect("Could not open input file."),
        );

        let expected_lines = BufReader::new(
            File::open("test_files/test_expected_output.gcode")
                .expect("Could not open input file."),
        )
        .lines()
        .map(|s| s.unwrap());

        for comparison in actual_lines.zip(expected_lines).enumerate() {
            let line_number = comparison.0 + 1;

            // Now make sure that each line of the actual output equals each
            // line of the generated output.
            assert_eq!(
                (comparison.1).0,
                (comparison.1).1,
                "Line {} differs from expected output.",
                line_number
            );
        }

        Result::Ok(())
    }
}
